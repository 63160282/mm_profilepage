
import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
                color: Colors.black
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo.shade500
        )
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark
    );
  }
}
class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheam = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheam == APP_THEME.DARK
      ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),
      home: Scaffold(
          appBar: buildAppBarWidget(),
          body: buildBodyWidget(),
          floatingActionButton: FloatingActionButton(
          child : Icon(Icons.threesixty),
            onPressed: () {
              setState(() {
                currentTheam == APP_THEME.DARK
                ? currentTheam = APP_THEME.LIGHT
                : currentTheam = APP_THEME.DARK ;
              });
          },
        ),
      ),
    );
  }
}
Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDiretionsButton(),
      buildPayButton(),
    ],
  );
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
     //     color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
     //     color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
   //       color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
    //      color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDiretionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
     //     color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_outlined,
   //       color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text('0962826081'),
    subtitle: Text('mobile'),
    trailing:IconButton(
      icon: Icon(Icons.message),
      color: Colors.deepPurpleAccent,
      onPressed: (){},
    ),
  );

}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text('440-440-3390'),
    subtitle: Text('other'),
    trailing:IconButton(
      icon: Icon(Icons.message),
      color: Colors.lightGreenAccent,
      onPressed: (){},
    ),
  );

}
Widget emailListTile(){
  return ListTile(
      leading: Icon(Icons.email),
      title: Text('63160282@go.buu.ac.th'),
      subtitle: Text('work'),
      trailing: Text("")

  );

}
Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text('Bang saen'),
    subtitle: Text('home'),
    trailing: IconButton(
      icon: Icon(Icons.assistant_direction),
      color: Colors.cyanAccent,
      onPressed: (){},
    ),

  );

}
AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.cyan,

    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(Icons.star_border),
          color: Colors.white
      )
    ],
  );
}
Widget buildBodyWidget(){
  return ListView(

    children: <Widget>[
      Column(
        children:  <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child:

            Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/299752789_1760403337646418_5672092613041181655_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeGiJXJ9AUWdW9GK3ZnQV6ZxPqMim91rLUg-oyKb3WstSOV8ncvfcNFncuD1w9BRhHAfW8nltOJe22ThOx30dIxv&_nc_ohc=YmsR997GqrUAX9Zk9RN&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfB313PFUus8BYpD_yy9tjq_UOBMcDQt_JqVDQtlS20hzg&oe=63A7ADFA",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(" Jirarat Phoophamorn",
                    style : TextStyle(fontSize: 30),

                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.blue,
                ),
              ),
              child : profileActionItem(),
            ),
          ),


          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color:  Colors.grey,

          ),
          emailListTile(),
          addressListTile()
        ],
      )
    ],
  );
}